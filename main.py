''' Guess The Number Game !!'''
import random

def game(input_number):
    ''' Core Game Function'''
    number = random.randint(0,input_number)
    counter = 1
    while True:
        guessed_number = int(input("Guess the Number!!!!: "))
        if not isinstance(guessed_number,int) :
            print("Just Integer!!!!!!")
            continue
        if guessed_number < 0 :
            print ("Just Positive Integer!!!!!")
            continue
        if number > guessed_number:
            print("Please guess higher!!")
            counter += 1
            continue
        if number < guessed_number:
            print("Please guess little!!")
            counter += 1
            continue
        if number == guessed_number:
            print(f"Yeeee, You guessed the number!!! Your prediction counter is {counter} ")
            print("Number is",number)
            break


if __name__ == "__main__":
    while True:
        random_input = int(input("Please enter positive integer for guess!: "))
        if random_input > 0 :
            break
        print("Please just positive!!!")
        continue
    game(random_input)
